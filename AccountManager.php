<?php

require_once('Data/Accounts.php');
require_once('Data/Transactions.php');
require_once('Transactions/Transfer.php');

class AccountManager
{
    private $accounts;
    private $transactions;

    public function __construct()
    {
        $this->accounts = new Accounts();
        $this->transactions = new Transactions();
    }

    public function get($id)
    {
        return $this->accounts->getOne($id);
    }

    public function getAll()
    {
        return $this->accounts->list();
    }

    public function getBalance($id)
    {
        return $this->accounts->balance($id);
    }

    public function getTransactions($sort, $dir)
    {
        return $this->transactions->list($sort, $dir);
    }

    public function transact($account, $data)
    {
        $transaction = new Transfer($data['comment'], $data['amount'], $data['dueDate']);

        if ($transaction->balanceCheck($this->getBalance($account['id']), $data['amount'])) {
            return $transaction->operate($account['iban'], $data['receiver']);
        }
        return 'Not enough balance';
    }
}