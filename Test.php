<?php

require_once('AccountManager.php');


$accountManager = new AccountManager();


//    echo "- Get all: <br/>";
//    echo "<pre>";
//    print_r($accountManager->getAll());
//    echo "</pre>";
//
//
//    echo "<br/><br/>";
//
//
//    echo "- Get balance: <br/>";
//    echo 'balance: ' . $accountManager->getBalance(23);
//
//
//    echo "<br/><br/>";
//
//
//    echo "- Get balance: <br/>";
//    echo 'balance: ' . $accountManager->getBalance(23);
//
//
//    echo "<br/><br/>";
//
//
//    echo "- Get all account transactions(comment): <br/>";
//    echo "<pre>";
//    print_r($accountManager->getTransactions('comment', 'asc'));
//    echo "</pre>";
//
//
//    echo "<br/><br/>";
//
//
//    echo "- Get all account transactions(date): <br/>";
//    echo "<pre>";
//    print_r($accountManager->getTransactions('date', 'asc'));
//    echo "</pre>";
//
//
//    echo "<br/><br/>";


    $account = $accountManager->get(11);

    if ($account) {
        $data = [
            'comment' => 'test',
            'amount' => 800,
            'dueDate' => '2023-04-20 00:00:00',
            'receiver' => 'zzzz',
        ];

        echo $accountManager->transact($account, $data);
    }
    else {
        echo 'Account not found.';
    }

