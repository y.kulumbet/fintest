<?php


interface TransactionInterface
{
    public function balanceCheck(float $senderBalance, float $amount);
}