<?php

require_once('Transactions/TransactionInterface.php');

abstract class Transaction implements TransactionInterface
{
    protected $comment;
    protected $amount;
    protected $dueDate;

    abstract public function operate($fromIBAN, $toIBAN);

    public function __construct($comment, $amount, $dueDate)
    {
        $this->comment = $comment;
        $this->amount = $amount;
        $this->dueDate = $dueDate;
    }

    public function balanceCheck(float $senderBalance, float $amount)
    {
        return $senderBalance >= $amount;
    }
}