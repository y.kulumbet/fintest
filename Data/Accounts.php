<?php


class Accounts
{
    private $list = [];
    private $balances = [];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->list = [
            11 => ['id' => 11, 'name' => 'Jack Richard', 'iban' => 'xxxx', 'deposite' => 'yyyy'],
            12 => ['id' => 12, 'name' => 'John Doe', 'iban' => 'xxxx', 'deposite' => 'yyyy'],
            23 => ['id' => 23, 'name' => 'Kate Hudson', 'iban' => 'xxxx', 'deposite' => 'yyyy'],
            44 => ['id' => 44, 'name' => 'Tom Hard', 'iban' => 'xxxx', 'deposite' => 'yyyy'],
            75 => ['id' => 75, 'name' => 'Anne Shephard', 'iban' => 'xxxx', 'deposite' => 'yyyy'],
        ];

        $this->balances = [
            11 => 1900,
            12 => 431,
            23 => 6012,
            44 => 221,
            75 => 5000,
        ];
    }

    public function list()
    {
        return $this->list;
    }

    public function getOne($id)
    {
        return isset($this->list[$id]) ? $this->list[$id] : null;
    }

    public function balance($id)
    {
        return $this->balances[$id];
    }
}