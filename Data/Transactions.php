<?php


class Transactions
{
    private $list = [];

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $this->list = [
            ['comment' => 'Test 1', 'balance' => 1000, 'date' => '2023-04-27', 'type' => 0],
            ['comment' => 'Test 2', 'balance' =>500, 'date' => '2023-04-20', 'type' => 1],
            ['comment' => 'Test 3', 'balance' =>2000, 'date' => '2023-04-22', 'type' => 1],
            ['comment' => 'Test 4', 'balance' =>100, 'date' => '2023-04-21', 'type' => 0],
            ['comment' => 'Test 5', 'balance' =>650, 'date' => '2023-04-20', 'type' => 2],
        ];
    }

    public function list($sort, $dir)
    {
        $dir = ($dir === 'asc') ? SORT_ASC : SORT_DESC;

        if ($sort === 'comment') {
            array_multisort(array_column($this->list, 'comment'), $dir, $this->list);
        } else {
            array_multisort(array_column($this->list, 'date'), $dir, $this->list);
        }

        return $this->list;
    }
}